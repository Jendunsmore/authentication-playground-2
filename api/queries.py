from psycopg.rows import class_row
from psycopg_pool import ConnectionPool
import os
from models import AccountIn, AccountOutWithHashedPassword
from models import AuthenticationException

pool = ConnectionPool(conninfo=os.environ["DATABASE_URL"])


class AccountRepo:
    def create(
        self, account: AccountIn, hashed_password: str
    ) -> AccountOutWithHashedPassword:
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute(
                    """
                    INSERT INTO accounts
                        (username, email, hashed_password)
                    VALUES
                        (%s, %s, %s)
                    RETURNING *;
                    """,
                    [
                        account.username,
                        account.email,
                        hashed_password,
                    ],
                )
                id = cur.fetchone()[0]
                account_data = {
                    **account.dict(),
                    "hashed_password": hashed_password,
                }
                if not account_data:
                    raise AuthenticationException("Error creating Account")
                return AccountOutWithHashedPassword(id=id, **account_data)

    def get(self, username: str) -> AccountOutWithHashedPassword:
        with pool.connection() as conn:
            with conn.cursor(
                row_factory=class_row(AccountOutWithHashedPassword)
            ) as cur:
                cur.execute(
                    """
                    SELECT id, username, email, hashed_password
                    FROM accounts
                    WHERE username = %s;
                    """,
                    [username],
                )
                return cur.fetchone()

    def get_user_by_id(self, pk: int) -> AccountOutWithHashedPassword:
        with pool.connection() as conn:
            with conn.cursor(
                row_factory=class_row(AccountOutWithHashedPassword)
            ) as cur:
                cur.execute(
                    """
                    SELECT id, username, email, hashed_password
                    FROM accounts
                    WHERE id = %s;
                    """,
                    [pk],
                )
                ac = cur.fetchone()
                if ac is None:
                    raise AuthenticationException("No account found")
                else:
                    return ac
