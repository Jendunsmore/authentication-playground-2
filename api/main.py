from fastapi import FastAPI, Depends, HTTPException, Request, Response, status
from fastapi.middleware.cors import CORSMiddleware
import os
from models import AccountOut, AccountIn, AccountToken
from queries import AccountRepo
from authenticator import MyAuthenticator
from models import AuthenticationException

authenticator = MyAuthenticator(os.environ["SIGNING_KEY"])
app = FastAPI()

origins = [
    "http://localhost:3000",
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

app.include_router(authenticator.router)


@app.get("/token", response_model=AccountToken | None)
async def get_token(
    request: Request,
    account: AccountOut = Depends(authenticator.try_get_current_account_data),
) -> AccountToken | None:
    if account and authenticator.cookie_name in request.cookies:
        token = AccountToken(
            access_token=request.cookies[authenticator.cookie_name],
            type="Bearer",
            account=account,
        )
        return token


# Creates a new user
@app.post("/api/account", response_model=AccountToken)
async def create_user(
    info: AccountIn,
    request: Request,
    response: Response,
    repo: AccountRepo = Depends(),
):
    hashed_password = authenticator.hash_password(info.password)
    try:
        account = repo.create(info, hashed_password)
    except Exception as e:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail=str(e),
        )
    token = await authenticator.login(response, request, info, repo)
    return AccountToken(account=account, **token.dict())


@app.get("/api/user/{pk}")
async def get_user(
    pk: int,
    accounts: AccountRepo = Depends(),
    ra=Depends(authenticator.get_current_account_data),
) -> AccountOut:
    if not ra:
        raise HTTPException(status.HTTP_401_UNAUTHORIZED)
    try:
        account = accounts.get_user_by_id(pk)
    except AuthenticationException:
        raise HTTPException(status.HTTP_401_UNAUTHORIZED)
    return AccountOut(
        id=account.id,
        username=account.username,
        email=account.email,
    )


# Example protected endpoint
@app.get("/api/protected", response_model=bool)
async def get_protected(
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    return True


# Verifies that the user is logged in
# @app.get("/token")
# async def get_by_cookie(
#     request: Request,
#     account_data: dict | None = Depends(
#         authenticator.try_get_current_account_data
#     ),
#     accounts: AccountRepo = Depends(),
#     ra=Depends(authenticator.get_current_account_data),
# ) -> AccountToken:
#     if not account_data:
#         raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED)
#     account = await get_user(account_data["id"], accounts=accounts, ra=ra)

#     account_token = {
#         "access_token": request.cookies[authenticator.cookie_name],
#         "type": "Bearer",
#         "account": account,
#     }
#     return AccountToken(**account_token)
